/**
 * @file data.h
 * @brief some very basic data manipulation functions
 *
 *
 * @author manthan shah
 * @date Sept 11 2020
 */

#ifndef __INCLUDE_DATA_H__
#define __INCLUDE_DATA_H__
#include <stdint.h>
/**
 * @brief	Converts data from a standard integer type into an ASCII string.
 *			
 *	Null terminated strings.
 *
 * @param ptr resultant ascii array
 * @param data The number you wish to convert is passed in as a signed 32-bit integer.
 * @param base Supports bases 2 to 16 
 *
 * @return the length of the converted data (including a negative sign)
 */
uint8_t my_itoa(int32_t data, uint8_t * ptr, uint32_t base);


/**
 * @brief	Converts data back from an ASCII represented string into an integer type.
 *			
 *	Null terminated strings.
 *
 * @param ptr The character string to convert
 * @param digits The number of digits in the character set
 * @param base Supports bases 2 to 16 
 *
 * @return the number
 */
int32_t my_atoi(uint8_t * ptr, uint8_t digits, uint32_t base);

#endif
