/*************************************************************************************
 *                                                                                   *
 * Assignment 1 submission                                                           *
 *                                                                                   *
 * author : Manthan Shah                                                             *
 *                                                                                   *
 * date : 27,August,2020                                                             *
 *                                                                                   *
 * This project is a submission of the final assessment for the course Introduction  *
 * to Embedded  Systems by University of Colorado.                                   *
 *                                                                                   *
 *************************************************************************************/

If you face any issues during the build process, you can type "make info" for some 
general info. 