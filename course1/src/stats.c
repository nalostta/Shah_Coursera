/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material.
 *
 *****************************************************************************/
/**
 * @file	:	stats.c
 * @brief	:	implementation file for a couple of stat functions 
 *
 * The functions written here can analyze an array of unsigned char data items and report analytics on the maximum, minimum, mean, and median of a given data set.
 * There are also functions of ordering the elements of the dataset into ascending or descending order along with print functions.
 *
 * @author	:	manthan shah
 * @date	:	August 27th,2020
 *
 */



#include <stdio.h>
#include "stats.h"
#include "platform.h"

/* Size of the Data Set */
#define SIZE (40)



//Stat function definitions :

unsigned char find_maximum(const unsigned char* src,unsigned int max_size,unsigned int isSorted,int* status)
{
	int _status = STATUS_OK;
	int max=0;
	
	if(src==NULL)
	{
		_status = STATUS_NULL_ARG;
	}else
	{
		if(isSorted==BOOL_TRUE)
		{
			max = ((src[max_size-1]>src[0])? src[max_size-1]:src[0]);
		}else
		{
			for(int i=0;i<max_size;i++)
			{
				max = ((max>=src[i])?	max:src[i]);
			}
		}
	}
	
	if(status!=NULL) *status = _status;
	
	return max;
}

unsigned char find_minimum(const unsigned char* src,unsigned int max_size,unsigned int isSorted,int* status)
{
	int _status = STATUS_OK;
	int min=0;
	
	if(src==NULL)
	{
		_status = STATUS_NULL_ARG;
	}else
	{
		if(isSorted==BOOL_TRUE)
		{
			min = ((src[max_size-1]<src[0])? src[max_size-1]:src[0]);
		}else
		{
			for(int i=0;i<max_size;i++)
			{
				min = ((min<=src[i])?	min:src[i]);
			}
		}
	}
	
	if(status!=NULL) *status = _status;
	
	return min;
}

unsigned char find_mean(const unsigned char* src,unsigned int max_size,int* status)
{
	int _status = STATUS_OK;
	int mean=0;
	
	if(src==NULL)
	{
		_status = STATUS_NULL_ARG;
	}else
	{
		if(max_size==0)
		{
			_status = STATUS_INVALID_ARG;
		}else
		{
			for(int i=0;i<max_size;i++)
			{
				mean+=src[i];
			}
		}
		
	}
	
	if(status!=NULL) *status = _status;
	return ((double)mean/(double)max_size); 
}

unsigned char find_median(unsigned char* src,unsigned int max_size,unsigned int isSorted,int* status)
{
	int _status = STATUS_OK;
	unsigned char median=0;

	if(src==NULL)
	{
		_status = STATUS_NULL_ARG;
	}else
	{
		if(isSorted==BOOL_TRUE)
		{			
			median = src[max_size/2];
		}else
		{
			PRINTF("\n[info] : array unsorted, sorting in ascending order");
			if(sort_array(src,max_size,'a')==STATUS_OK)
			{
				int mid = max_size/2;
				if(max_size%2==0)
				{
					median = src[mid+1];
				}else
				{
					median = (((double)src[mid]+(double)src[mid+1])/(double)2);  //double for explicit typecast
				}
				
			}else
			{
				_status = STATUS_ERROR;
			}	
		}
	}
	if(status!=NULL) *status = _status;
	return median;
}

int sort_array(unsigned char* src,unsigned int max_size,char sortType)
{
	if(src==NULL)
	{
		return STATUS_ERROR;
	}else
	{
		if((sortType!='a')&&(sortType!='d'))
		{
			return STATUS_INVALID_ARG;
		}else
		{
			int i,j;
			for(i=0;i<max_size;i++)
			{
				for(j=0;j<max_size-1;j++)
				{
					if(((sortType=='a')&&(src[j]>src[j+1]))||((sortType=='d')&&(src[j]<src[j+1])))
					{
						int temp = src[j];
						src[j] = src[j+1];
						src[j+1] = temp;
					}
				}
			}
		}
	}
	return STATUS_OK;
}

void print_statistics(unsigned char* src,unsigned int max_size,int isSorted,int* status)
{
	PRINTF("\n\n-------array stats-------\nmedian\t: %d",find_median(src,max_size,isSorted,status));
	PRINTF("\nmean\t: %d",find_mean(src,max_size,status));
	
	if(*status==STATUS_OK)
	{
		PRINTF("\nmin val\t: %d",find_minimum(src,max_size,BOOL_TRUE,status));
		PRINTF("\nmax val\t: %d\n-------------------------\n",find_maximum(src,max_size,BOOL_TRUE,status));
	}else
	{
		PRINTF("\nmin val\t: %d",find_minimum(src,max_size,BOOL_FALSE,status));
		PRINTF("\nmax val\t: %d\n-------------------------",find_maximum(src,max_size,BOOL_FALSE,status));
	}
	return;
}

void print_array(const unsigned char* src,unsigned int max_size)
{
	PRINTF("\narray:\n");
	for(int i=0;i<max_size;i++) PRINTF(" %d",(int)src[i]);
	return;
}

