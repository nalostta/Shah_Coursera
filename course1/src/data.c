#include "data.h"
#include "memory.h"

//max ptr size = 12, null included
uint8_t my_itoa(int32_t data, uint8_t * ptr, uint32_t base)
{
	int len=0;
	if((ptr!=NULL)&&(base<=16))
	{
		//check sign
		//rem = data
		//Q = data
		int32_t Q = data;
		int32_t i=0;
		int8_t neg = 0;
		
		if(data<0) 
		{
			neg=1;
			Q = -Q;
		}

		// LOOP TILL Q!=0
		// rem = Q%base -> buf[i++]
		// Q = Q/base
		while(Q!=0&&i<11)
		{
			
			*(ptr+i) = Q%base;
			Q = Q/base;
			i++;
		}
		
		//add - sign if negative
		if(neg)
		{
			*(ptr+i)='-';
			i++;
		}	
		
		//reverse string
		my_reverse(ptr,i);
		len=i;
		
		//add null
		*(ptr+i)=0;
		
		for(i=0;i<len;i++)
		{
			if(ptr[i]!='-')
			{
				if(ptr[i]>9)
				{
					ptr[i] += 55;
				}else
				{
					ptr[i] += '0';
				}
			}
		}
	}
	return len;
}

int32_t my_atoi(uint8_t * ptr, uint8_t digits, uint32_t base)
{
	int32_t num=0;
	if((ptr!=NULL)&&(base<=16)&&(digits<=12))
	{
		uint8_t neg=0;
		if(ptr[0]=='-') {neg++;}
		
		int32_t i,temp;
		for(i=neg;i<digits;i++)
		{
			temp = (ptr[i]<'9')? (ptr[i]-'0'):(ptr[i]-55);
			num *= base;
			num += temp;
		}
		
		if(neg) {num = -num;}
	}
	return num;
}
