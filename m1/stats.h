/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file : stats.h
 * @brief: headers for the stats module (stats.c).
 *
 * The functions written here can analyze an array of unsigned char data items and report analytics on the maximum, minimum, mean, and median of a given data set.
 * There are also functions of ordering the elements of the dataset into ascending or descending order along with print functions.
 *
 * @author	:	manthan shah
 * @date	:	August 27th,2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 
#define	STATUS_OK 0
#define	STATUS_ERROR		(STATUS_OK-1)
#define	STATUS_NULL_ARG		(STATUS_ERROR-1)
#define	STATUS_INVALID_ARG	(STATUS_NULL_ARG-1)

#define BOOL_TRUE 1
#define BOOL_FALSE 0


/**
 * @brief finds maximum in an array of type unsigned char
 *
 * sets the status NULL_ARG if src is a nullptr
 *
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e. max_size-1).
 * @param isSorted : input macros BOOL_TRUE/BOOL false used here. if the array is sorted, it returns the maximum of the first and last element else it iterates through the array
 * @param status   : pointer to the status variable used to set the status of the operation by the function.can be null.
 *
 * @return maximum value in the array.
 */
unsigned char find_maximum(const unsigned char* src,unsigned int max_size,unsigned int isSorted,int* status);


/**
 * @brief finds minimum in an array of type unsigned char
 *
 * sets the status NULL_ARG if src is a nullptr
 *
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e. max_size-1).
 * @param isSorted : input macros BOOL_TRUE/BOOL false used here. if the array is sorted, it returns the minimum of the first and last element else it iterates through the array
 * @param status   : pointer to the status variable used to set the status of the operation by the function.can be null.
 *
 * @return maximum value in the array.
 */
unsigned char find_minimum(const unsigned char* src,unsigned int max_size,unsigned int isSorted,int* status);


/**
 * @brief finds the mean in an array of type unsigned char
 *
 * sets the status NULL_ARG if src is a nullptr
 *
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e.max_size-1).
 * @param status   : pointer to the status variable used to set the status of the operation by the function.can be null.
 *
 * @return floor of the mean value (implicitly casted to unsigned char).
 */
unsigned char find_mean(const unsigned char* src,unsigned int max_size,int* status);



/**
 * @brief finds minimum in an array of type unsigned char
 *
 * sets the status NULL_ARG if src is a nullptr
 *
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e. max_size-1).
 * @param isSorted : input macros BOOL_TRUE/BOOL false used here. if the array is sorted, it returns the middle element otherwise it sorts the array in ascending order by default and then returns the middle element.
 * @param status   : pointer to the status variable used to set the status of the operation by the function.can be null.
 *
 * @return median value in the array.
 */
unsigned char find_median(unsigned char* src,unsigned int max_size,unsigned int isSorted,int* status);



/**
 * @brief finds minimum in an array of type unsigned char
 *
 * sets the status NULL_ARG if src is a nullptr. 
 * NOTE : This function will modify the src array!
 *
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e. max_size-1).
 * @param sortType : inputs 'a' or 'd' denoting ascending or descending sort type.For any other value, STATUS_ERROR is returned.
 *
 * @return status of the sorting operation.
 */
int sort_array(unsigned char* src,unsigned int max_size,char sortType);




/**
 * @brief prints max,min,mean and the median values of the input array.
 *
 * sets the status NULL_ARG if src is a nullptr.
 * NOTE : This function can modify the src array!
 *
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e.max_size-1).
 * @param isSorted : input macros BOOL_TRUE/BOOL false used here. if the array is not sorted, the median function will sort in ascending by default.
 * @param status   : pointer to the status variable used to set the status of the operation by the function.can be null.
 */
void print_statistics(unsigned char* src,unsigned int max_size,int isSorted,int* status);



/**
 * @brief prints the elements of the array
 * @param max_size : size of the array. Note : The function only scans till the index denoted by max_size (i.e.max_size-1).
 */
void print_array(const unsigned char* src,unsigned int max_size);


#endif /* __STATS_H__ */

