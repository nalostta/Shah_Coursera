/*************************************************************************************
 *                                                                                   *
 * Assignment 1 submission                                                           *
 *                                                                                   *
 * author : Manthan Shah                                                             *
 *                                                                                   *
 * date : 27,August,2020                                                             *
 *                                                                                   *
 * This project is a submission of assignment week 1 for the course Introduction to  *
 * Embedded  Systems by University of Colorado.                                      *
 * In this assignment a simple application is created that performs statistical      *
 * analytics on a dataset.                                                           *
 *                                                                                   *
 *************************************************************************************/

